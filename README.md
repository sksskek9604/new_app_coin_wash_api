## 코인세탁방 Android App

### * coin-wash-manager를 API로 사용하는 APP입니다.
### * 코인 세탁방의 세탁기/건조기 기계를 관리할 수 있습니다.

### 기능
> * 기계 등록
> * 기계 수정
> * 기계 상세정보
> * 기계 리스트

---

## Page_Index
> ##
> <center><img src="./images/page_index.png" width="450" height="900"></center>
>

## Page_Detail (기계 상세정보)
> ##
> <center><img src="./images/page_detail.png" width="450" height="900"></center>
>

## Page_Detail_Modify
> ##
> <center><img src="images/page_detail_modify.png" width="450" height="900"></center>
>

## Page_Form
> ##
> <center><img src="./images/page_form.png" width="450" height="900"></center>
>

import 'package:flutter/material.dart';

class ComponentAppbarNormal extends StatelessWidget with PreferredSizeWidget {
  const ComponentAppbarNormal({super.key, required this.title});

  final String title;

  @override
  Widget build(BuildContext context) {
    return AppBar(
      centerTitle: false,
      automaticallyImplyLeading: false,
      title: Text(title),
      elevation: 1.0,
    );
  }

  @override
  Size get preferredSize => const Size.fromHeight(45);
}

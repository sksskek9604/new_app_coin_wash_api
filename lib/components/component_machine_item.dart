import 'package:flutter/material.dart';
import 'package:new_app_coin_wash_api/model/machine_item.dart';

class ComponentMachineItem extends StatelessWidget {
  const ComponentMachineItem({super.key, required this.machineItem, required this.callback});

  final MachineItem machineItem;
  final VoidCallback callback;

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: callback,
      child: Container(
        padding: EdgeInsets.all(20),
        child: Column(
          children: [
            Container(
              padding: EdgeInsets.all(30),
              decoration: BoxDecoration(
                borderRadius: BorderRadius.circular(30),
                border: Border.all(width: 2.0, color: Colors.black12),
              ),
              child: Column(
                children: [
                  Text(machineItem.machineFullName),
                  Text('구매일 : ${machineItem.datePurchase}'),
                  SizedBox(height: 20,),
                  Text('Click here', style: TextStyle(fontSize:24,fontWeight: FontWeight.bold, color: Colors.teal.shade700),),
                ],
              ),
            ),
          ],
        ),
      ),
    );
  }
}

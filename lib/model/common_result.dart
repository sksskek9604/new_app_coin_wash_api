class CommonResult {
  bool isSuccess;
  int code;
  String msg;

  CommonResult(this.isSuccess, this.code, this.msg);

  factory CommonResult.fromJson(Map<String, dynamic> json) {
    return CommonResult(
      json['isSuccess'] as bool,
      json['code'],
      json['msg']
    );
  }
}
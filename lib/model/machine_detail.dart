class MachineDetail {
  int id;
  String machineTypeName;
  String machineName;
  String datePurchase;
  double machinePrice;

  MachineDetail(this.id, this.machineTypeName, this.machineName, this.datePurchase, this.machinePrice);

  factory MachineDetail.fromJson(Map<String, dynamic> json) {
    return MachineDetail(
      json['id'],
      json['machineTypeName'],
      json['machineName'],
      json['datePurchase'],
      json['machinePrice'],
    );
  }
}
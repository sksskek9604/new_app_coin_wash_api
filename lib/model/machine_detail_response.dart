
import 'package:new_app_coin_wash_api/model/machine_detail.dart';

class MachineDetailResponse {
  MachineDetail data;
  bool isSuccess;
  int code;
  String msg;

  MachineDetailResponse(this.data, this.isSuccess, this.code, this.msg);

  factory MachineDetailResponse.fromJson(Map<String, dynamic> json) {
    return MachineDetailResponse(
      MachineDetail.fromJson(json['data']),
      json['isSuccess'] as bool,
      json['code'],
      json['msg'],
    );
  }
}
class MachineItem {
  int id;
  String machineFullName;
  String datePurchase;

  MachineItem(this.id, this.machineFullName, this.datePurchase);

  factory MachineItem.fromJson(Map<String, dynamic> json) {
    return MachineItem(
      json['id'],
      json['machineFullName'],
      json['datePurchase'],
    );
  }
}
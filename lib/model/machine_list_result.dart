import 'package:new_app_coin_wash_api/model/machine_item.dart';

class MachineListResult {
  List<MachineItem> list;
  int totalItemCount;
  int totalPage;
  int currentPage;
  bool isSuccess;
  int code;
  String msg;

  MachineListResult(this.list, this.totalItemCount, this.totalPage, this.currentPage, this.isSuccess, this.code, this.msg);

  factory MachineListResult.fromJson(Map<String, dynamic> json) {
    return MachineListResult(
      json['list'] == null ? [] : (json['list'] as List).map((e) => MachineItem.fromJson(e)).toList(),
      json['totalItemCount'],
      json['totalPage'],
      json['currentPage'],
      json['isSuccess'] as bool,
      json['code'],
      json['msg'],
    );
  }
}
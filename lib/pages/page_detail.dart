import 'package:bot_toast/bot_toast.dart';
import 'package:flutter/material.dart';
import 'package:intl/intl.dart';
import 'package:new_app_coin_wash_api/components/component_appbar_popup.dart';
import 'package:new_app_coin_wash_api/components/component_custom_loading.dart';
import 'package:new_app_coin_wash_api/components/component_no_contents.dart';
import 'package:new_app_coin_wash_api/components/component_notification.dart';
import 'package:new_app_coin_wash_api/model/machine_detail.dart';
import 'package:new_app_coin_wash_api/pages/page_form.dart';
import 'package:new_app_coin_wash_api/repository/repo_machine.dart';

class PageDetail extends StatefulWidget {
  const PageDetail({super.key, required this.machineId});
  /*
  이 창이 호출될 때 머신아이디가 꼭 필요함.
  머신아이디를 받아와서 기계 정보 단일 조회(get) api를 다시 호출 할 것임.
   */
  final int machineId;

  @override
  State<PageDetail> createState() => _PageDetailState();
}

class _PageDetailState extends State<PageDetail> {
  // api에서 받아온 숫자를 예쁘게 보여주기 위한 포맷터. 100000 -> 100,000 이런식으로 바꿔줌.
  final NumberFormat nFormat = NumberFormat('#,###');

  // list는 초기값으로 [] 빈배열로 넣어주고 모델같은 경우는 이런식으로 초기화 해줌. 숫자는 0, 문자는 ''
  MachineDetail _detail = MachineDetail(0, '', '', '', 0);

  @override
  void initState() {
    super.initState();
    _getDetail();
  }

  // 이 창이 호출될 때 머신아이디를 받아왔으므로 그걸 이용해서 삭제 구현
  Future<void> _delData() async {
    BotToast.showCustomLoading(toastBuilder: (cancelFunc) {
      return ComponentCustomLoading(cancelFunc: cancelFunc);
    });

    await RepoMachine().delData(widget.machineId).then((res) {
      BotToast.closeAllLoading();

      ComponentNotification(
        success: true,
        title: '데이터 삭제 성공',
        subTitle: res.msg,
      ).call();

      Navigator.pop(context); // 다이알로그 창 닫기
      Navigator.pop(context, [true]); // 상세페이지 닫으면서 부모페이지(리스트페이지)에 새로고침 해야한다고 말해주기
    }).catchError((err) {
      BotToast.closeAllLoading();

      ComponentNotification(
        success: false,
        title: '데이터 삭제 실패',
        subTitle: '데이터 삭제에 실패하였습니다.',
      ).call();

      // 삭제가 실패했으므로 현재창 닫지 않음.
      Navigator.pop(context); // 다이알로그 창 닫기
    });
  }

  Future<void> _getDetail() async {
    BotToast.showCustomLoading(toastBuilder: (cancelFunc) {
      return ComponentCustomLoading(cancelFunc: cancelFunc);
    });

    await RepoMachine().getDetail(widget.machineId).then((res) {
      BotToast.closeAllLoading();

      setState(() {
        _detail = res.data;
      });
    }).catchError((err) {
      BotToast.closeAllLoading();

      ComponentNotification(
        success: false,
        title: '데이터 로딩 실패',
        subTitle: '데이터 로딩에 실패하였습니다.',
      ).call();

      // 호출실패 시 창을 닫아버림. (데이터가 없는 시퀀스 or 인터넷 오류로 이 창을 유지시켜봤자 의미가 없음.)
      Navigator.pop(context);
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: const ComponentAppbarPopup(
        title: '기계 상세정보',
      ),
      body: _buildBody(),
    );
  }

  Widget _buildBody() {
    // MachineDetail의 id가 0이 아니라면 (초기값을 0으로 줬기때문에 데이터 불러오기 전에는 0임)
    if (_detail.id != 0) {
      return Container(
        padding: EdgeInsets.all(30),
        child: ListView(
          children: [
            Text('고유번호 : ${_detail.id}'),
            Divider(),
            Text('기계이름 : ${_detail.machineName}'),
            Divider(),
            Text('구매금액 : ${nFormat.format(_detail.machinePrice)}'),
            Divider(),
            Text('기계타입 : ${_detail.machineTypeName}'),
            Divider(),
            Text('구매일 : ${_detail.datePurchase}'),
            SizedBox(height: 30,),
            OutlinedButton(
                onPressed: () async {
                  final popup = await Navigator.push(
                      context,
                      MaterialPageRoute(builder: (context) => PageForm(machineDetail: _detail,))
                  );

                  if (popup != null && popup[0]) { // 다 처리하고 정상적으로 닫혔다! -> 열렸던 창이 닫히면서 알려주면
                    // 수정 완료되면 이 창도 또 닫을거니까 다시 한번 정상적으로 닫혔다!! 라고 리스트페이지(부모)에게 알려주기
                    Navigator.pop(
                        context,
                        [true]
                    );
                  }
                },
                child: const Text('수정', style: TextStyle(fontSize: 22.0),)
            ),
            SizedBox(height: 5,),
            OutlinedButton(
                onPressed: () {
                  _showDeleteDialog();
                },
                child: const Text('삭제', style: TextStyle(fontSize: 22.0, color: Colors.red))
            ),
          ],
        ),
      );
    } else {
      return const ComponentNoContents(icon: Icons.not_interested, msg: '기계정보가 없습니다.');
    }
  }

  void _showDeleteDialog() {
    showDialog(
        context: context,
        barrierDismissible: true,
        builder: (BuildContext context) {
          return AlertDialog(
            title: const Text('기계정보 삭제'),
            content: const Text('정말 삭제하시겠습니까?'),
            actions: [
              OutlinedButton(
                  onPressed: () {
                    _delData();
                  },
                  child: const Text('확인')
              ),
              OutlinedButton(
                  onPressed: () {
                    Navigator.pop(context);
                  },
                  child: const Text('취소')
              ),
            ],
          );
        }
    );
  }
}

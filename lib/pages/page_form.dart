import 'package:bot_toast/bot_toast.dart';
import 'package:flutter/material.dart';
import 'package:flutter_form_builder/flutter_form_builder.dart';
import 'package:form_builder_validators/form_builder_validators.dart';
import 'package:intl/intl.dart';
import 'package:new_app_coin_wash_api/components/component_appbar_popup.dart';
import 'package:new_app_coin_wash_api/components/component_custom_loading.dart';
import 'package:new_app_coin_wash_api/components/component_notification.dart';
import 'package:new_app_coin_wash_api/config/config_dropdown.dart';
import 'package:new_app_coin_wash_api/config/config_form_validator.dart';
import 'package:new_app_coin_wash_api/model/machine_detail.dart';
import 'package:new_app_coin_wash_api/model/machine_request.dart';
import 'package:new_app_coin_wash_api/model/machine_update_request.dart';
import 'package:new_app_coin_wash_api/repository/repo_machine.dart';

class PageForm extends StatefulWidget {
  PageForm({super.key, this.machineDetail});

  // machineDetail 제공되지 않으면 등록, machineDetail 제공되면 수정
  // MachineDetail 모델 안에 기계시퀀스가 있기때문에 별도로 받지 않는다. (page_detail 참고)
  MachineDetail? machineDetail;

  @override
  State<PageForm> createState() => _PageFormState();
}

class _PageFormState extends State<PageForm> {
  final _formKey = GlobalKey<FormBuilderState>();

  /*
  수정에 id가 필요한 이유
  - 데이터 '이걸'로 수정해줘~ : ?? '이거'가 뭐지? 모르겠네! 그냥 모든 데이터 전체 다 이걸로 고치자
  - 5번 데이터 이걸로 수정해줘 : 아~ 데이터베이스에서 5번 데이터 가져와서 이걸로 고쳐야겠네.
   */
  Future<void> _putMachine(int id, MachineUpdateRequest request) async {
    // 여기서부터
    BotToast.showCustomLoading(toastBuilder: (cancelFunc) {
      return ComponentCustomLoading(cancelFunc: cancelFunc);
    });
    // 여기까지 커스텀로딩 띄우는 소스

    await RepoMachine().putData(id, request).then((res) {
      BotToast.closeAllLoading(); // 커스텀로딩 닫고

      ComponentNotification(
        success: true,
        title: '기계 수정 성공',
        subTitle: res.msg, // 백엔드에서 반환받은 메세지 넣기
      ).call();

      // 창 닫으면서 다 처리하고 정상적으로 닫혔다!! 라고 알려주기
      Navigator.pop(
          context,
          [true] // <- 이부분.. 다 처리하고 정상적으로 닫혔다!!
      );
    }).catchError((err) {
      BotToast.closeAllLoading(); // 커스텀로딩 닫고

      ComponentNotification(
        success: false,
        title: '기계 수정 실패',
        subTitle: '기계 수정에 실패하였습니다.',
      ).call();
    });
  }

  Future<void> _setMachine(MachineRequest request) async {
    // 여기서부터
    BotToast.showCustomLoading(toastBuilder: (cancelFunc) {
      return ComponentCustomLoading(cancelFunc: cancelFunc);
    });

    await RepoMachine().setData(request).then((res) {
      BotToast.closeAllLoading(); // 커스텀로딩 닫고

      ComponentNotification(
        success: true,
        title: '기계 등록 성공',
        subTitle: res.msg, // 백엔드에서 반환받은 메세지 넣기
      ).call();

      // 창 닫으면서 다 처리하고 정상적으로 닫혔다!! 라고 알려주기
      Navigator.pop(
        context,
        [true] // <- 이부분.. 다 처리하고 정상적으로 닫혔다!!
      );
    }).catchError((err) {
      BotToast.closeAllLoading(); // 커스텀로딩 닫고

      ComponentNotification(
        success: false,
        title: '기계 등록 실패',
        subTitle: '기계 등록에 실패하였습니다.',
      ).call();
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: ComponentAppbarPopup(title: '기계 ${widget.machineDetail == null ? '등록' : '수정'}'),
      body: _buildBody(),
      bottomNavigationBar: BottomAppBar(
        child: OutlinedButton(
          onPressed: () {
            if (_formKey.currentState?.saveAndValidate() ?? false) {
              if (widget.machineDetail == null) { // 등록모드라면

                MachineRequest request = MachineRequest(
                    _formKey.currentState!.fields['machineType']!.value,
                    _formKey.currentState!.fields['machineName']!.value,
                    DateFormat('yyyy-MM-dd').format(_formKey.currentState!.fields['datePurchase']!.value),
                    double.parse(_formKey.currentState!.fields['machinePrice']!.value)
                );

                _setMachine(request);
              } else { // 수정모드라면
                MachineUpdateRequest request = MachineUpdateRequest(
                  _formKey.currentState!.fields['machineName']!.value,
                );

                _putMachine(widget.machineDetail!.id, request);
              }
            }
          },
          child: Text(widget.machineDetail == null ? '등록' : '수정', style: TextStyle(fontSize: 24),),
        ),
      ),
    );
  }

  Widget _buildBody() {
    return SingleChildScrollView(
      child: FormBuilder(
        key: _formKey,
        autovalidateMode: AutovalidateMode.disabled,
        initialValue: widget.machineDetail == null ? {} : {
          'machineName': widget.machineDetail!.machineName,
        },
        child: Container(
          padding: EdgeInsets.all(30),
          child: Column(
            children: [
              FormBuilderTextField(
                name: 'machineName',
                decoration: const InputDecoration(
                  labelText: '기계이름',
                ),
                validator: FormBuilderValidators.compose([
                  FormBuilderValidators.required(errorText: formErrorRequired),
                  FormBuilderValidators.minLength(2, errorText: formErrorMinLength(2)),
                  FormBuilderValidators.maxLength(15, errorText: formErrorMaxLength(15)),
                ]),
                keyboardType: TextInputType.text,
              ),
              // 등록모드일때 '입력창' 형태로 넣기
              if (widget.machineDetail == null)
                FormBuilderTextField(
                  name: 'machinePrice',
                  decoration: const InputDecoration(
                    labelText: '기계가격',
                  ),
                  readOnly: widget.machineDetail == null ? false : true,
                  validator: FormBuilderValidators.compose([
                    FormBuilderValidators.required(errorText: formErrorRequired),
                    FormBuilderValidators.numeric(errorText: formErrorNumeric),
                  ]),
                  keyboardType: TextInputType.number,
                ),
              if (widget.machineDetail == null)
                FormBuilderDropdown<String>(
                  name: 'machineType',
                  decoration: const InputDecoration(
                    labelText: '기계종류',
                  ),
                  validator: FormBuilderValidators.compose(
                      [FormBuilderValidators.required(errorText: formErrorRequired)]),
                  items: dropdownMachineType,
                ),
              if (widget.machineDetail == null)
                FormBuilderDateTimePicker(
                  name: 'datePurchase',
                  format: DateFormat('yyyy-MM-dd'),
                  inputType: InputType.date,
                  decoration: InputDecoration(
                    labelText: '구매일',
                    suffixIcon: IconButton(
                      icon: const Icon(Icons.close),
                      onPressed: () {
                        _formKey.currentState!.fields['datePurchase']?.didChange(null);
                      },
                    ),
                  ),
                  locale: const Locale.fromSubtags(languageCode: 'ko'), // 한국
                  validator: FormBuilderValidators.compose(
                      [FormBuilderValidators.required(errorText: formErrorRequired)]
                  ),
                ),
              // 수정모드일땐 뿌리기만 하기
              if (widget.machineDetail != null)
                Text('기계가격 : ${widget.machineDetail!.machinePrice}'),
              if (widget.machineDetail != null)
                Text('기계종류 : ${widget.machineDetail!.machineTypeName}'),
              if (widget.machineDetail != null)
                Text('구매일 : ${widget.machineDetail!.datePurchase}'),
            ],
          ),
        )
      ),
    );
  }
}

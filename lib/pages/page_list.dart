import 'package:bot_toast/bot_toast.dart';
import 'package:flutter/material.dart';
import 'package:new_app_coin_wash_api/components/component_appbar_actions.dart';
import 'package:new_app_coin_wash_api/components/component_count_title.dart';
import 'package:new_app_coin_wash_api/components/component_custom_loading.dart';
import 'package:new_app_coin_wash_api/components/component_machine_item.dart';
import 'package:new_app_coin_wash_api/components/component_no_contents.dart';
import 'package:new_app_coin_wash_api/components/component_notification.dart';
import 'package:new_app_coin_wash_api/model/machine_item.dart';
import 'package:new_app_coin_wash_api/pages/page_detail.dart';
import 'package:new_app_coin_wash_api/pages/page_form.dart';
import 'package:new_app_coin_wash_api/repository/repo_machine.dart';

class PageList extends StatefulWidget {
  const PageList({Key? key}) : super(key: key);

  @override
  State<PageList> createState() => _PageListState();
}

class _PageListState extends State<PageList> {
  final _scrollController = ScrollController();

  List<MachineItem> _list = [];
  int _totalItemCount = 0;

  @override
  void initState() {
    super.initState();
    _getList();
  }

  Future<void> _getList() async {
    BotToast.showCustomLoading(toastBuilder: (cancelFunc) {
      return ComponentCustomLoading(cancelFunc: cancelFunc);
    });

    await RepoMachine().getList().then((res) {
      BotToast.closeAllLoading();

      setState(() {
        _list = res.list;
        _totalItemCount = res.totalItemCount;
      });
    }).catchError((err) {
      BotToast.closeAllLoading();

      // 이번에 추가된 알림창 컴포넌트. component_notification 참고
      ComponentNotification(
        success: false,
        title: '데이터 로딩 실패',
        subTitle: '데이터 로딩에 실패하였습니다.',
      ).call();
    });
  }

  @override
  Widget build(BuildContext context) {
    return Stack(
      children: [
        Container(
          decoration: BoxDecoration(
              image: DecorationImage(
                  opacity: 1.0,
                  fit: BoxFit.fill,
                  image: AssetImage('assets/coin_wash_background.png'))),
          child: Scaffold(
            backgroundColor: Colors.transparent,
            appBar: ComponentAppbarActions(
              title: '기계 리스트',
              isUseActionBtn1: true,
              action1Icon: Icons.add,
              action1Callback: () async {
                final popup = await Navigator.push(
                    context,
                    MaterialPageRoute(builder: (context) => PageForm())
                );

                if (popup != null && popup[0]) { // 다 처리하고 정상적으로 닫혔다!! 라고 열렸던 창이 닫히면서 알려주면
                  _getList(); // 리스트를 새로 불러온다. 왜? 등록 혹은 수정 하면 데이터가 변경되었으니
                  // 데이터를 반드시 새로 불러와서 그려줘야한다.
                }
              },
              isUseActionBtn2: true,
              action2Icon: Icons.refresh,
              action2Callback: () {
                _getList();
              },
            ),
            body: ListView(
              controller: _scrollController,
              children: [
                ComponentCountTitle(icon: Icons.list, count: _totalItemCount, unitName: '대', itemName: '세탁기계'),
                _buildList(),
              ],
            ),
          ),
        )
      ],
    );
  }

  Widget _buildList() {
    if (_totalItemCount > 0) {
      return Column(
        mainAxisSize: MainAxisSize.min,
        crossAxisAlignment: CrossAxisAlignment.stretch,
        children: [
          ListView.builder(
            physics: const NeverScrollableScrollPhysics(),
            shrinkWrap: true,
            itemCount: _list.length,
            itemBuilder: (_, index) => ComponentMachineItem(
              machineItem: _list[index],
              callback: () async {
                final popup = await Navigator.push(
                    context,
                    MaterialPageRoute(builder: (context) => PageDetail(machineId: _list[index].id))
                );

                // 상세페이지에서 수정이나 삭제 완료하고 닫혔다!! 라고 알려주면 리스트 갱신하기
                if (popup != null && popup[0]) {
                  _getList();
                }
              },
            ),
          ),
        ],
      );
    } else {
      return SizedBox(
        height: MediaQuery.of(context).size.height - 45,
        child: const ComponentNoContents(icon: Icons.not_interested, msg: '데이터가 없습니다.'),
      );
    }
  }
}

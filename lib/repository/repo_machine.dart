import 'package:dio/dio.dart';
import 'package:new_app_coin_wash_api/config/config_api.dart';
import 'package:new_app_coin_wash_api/model/common_result.dart';
import 'package:new_app_coin_wash_api/model/machine_detail_response.dart';
import 'package:new_app_coin_wash_api/model/machine_list_result.dart';
import 'package:new_app_coin_wash_api/model/machine_request.dart';
import 'package:new_app_coin_wash_api/model/machine_update_request.dart';

class RepoMachine {
  Future<CommonResult> setData(MachineRequest request) async {
    const String baseUrl = '$apiUri/machine/new';

    Dio dio = Dio();

    final response = await dio.post(
      baseUrl,
      data: request.toJson(),
      options: Options(
        followRedirects: false,
        validateStatus: (status) {
          return status == 200;
        }
      )
    );

    return CommonResult.fromJson(response.data);
  }

  Future<CommonResult> putData(int id, MachineUpdateRequest request) async {
    const String baseUrl = '$apiUri/machine/put-name/{id}';

    Dio dio = Dio();

    final response = await dio.put(
        baseUrl.replaceAll('{id}', id.toString()),
        data: request.toJson(),
        options: Options(
            followRedirects: false,
            validateStatus: (status) {
              return status == 200;
            }
        )
    );

    return CommonResult.fromJson(response.data);
  }

  // 삭제 api는 없지만 예시코드로 넣어둠.
  Future<CommonResult> delData(int id) async {
    const String baseUrl = '$apiUri/machine/{id}';

    Dio dio = Dio();

    final response = await dio.delete(
        baseUrl.replaceAll('{id}', id.toString()),
        options: Options(
            followRedirects: false,
            validateStatus: (status) {
              return status == 200;
            }
        )
    );

    return CommonResult.fromJson(response.data);
  }

  Future<MachineListResult> getList({String machineType = ''}) async {
    const String baseUrl = '$apiUri/machine/search';

    Map<String, dynamic> params = {};
    if (machineType != '') params['machineType'] = machineType;

    Dio dio = Dio();

    final response = await dio.get(
      baseUrl,
      queryParameters: params,
      options: Options(
          followRedirects: false,
          validateStatus: (status) {
            return status == 200;
          }
      )
    );

    return MachineListResult.fromJson(response.data);
  }

  Future<MachineDetailResponse> getDetail(int machineId) async {
    const String baseUrl = '$apiUri/machine/{id}';

    Dio dio = Dio();

    final response = await dio.get(
        baseUrl.replaceAll('{id}', machineId.toString()),
        options: Options(
            followRedirects: false,
            validateStatus: (status) {
              return status == 200;
            }
        )
    );

    return MachineDetailResponse.fromJson(response.data);
  }
}